#!/bin/bash

# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

#update system
sudo yum -y update

#install java 8
cd ~
sudo yum -y install wget
wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.rpm
sudo yum -y localinstall jdk-8u131-linux-x64.rpm
java -version

#cd /etc/security
#sudo chmod u+w limits.conf

#Install MySQL Database
sudo wget http://dev.mysql.com/get/mysql57-community-release-el7-8.noarch.rpm
sudo yum -y install mysql57-community-release-el7-8.noarch.rpm

#Set MySQL Version
sudo yum-config-manager --disable mysql57-community
sudo yum-config-manager --enable mysql56-community
sudo yum -y install mysql mysql-server mysql-devel

#Enable mysqld
sudo systemctl enable mysqld
sudo systemctl start mysqld

#Configure and init the MySQL database
sudo echo "[mysql]
user=root
password=
[mysqldump]
user=root
password=" > ~/.my.cnf

sudo mysql < /tmp/raspManager-db-init.sql

#Install the RASP Manager RPM
sudo yum -y localinstall /tmp/rasp-manager.rpm

#Change the RASP Manager configuration

sudo cp /tmp/config.json /etc/prevoty/manager/config.json

#Init the RASP Manager database
sudo mysql -D prevoty < /tmp/raspManager-db-load-config.sql

#Install nginx
sudo yum -y  install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum -y install nginx
sudo cp /tmp/nginx.conf /etc/nginx/nginx.conf
sudo chmod 0644 /etc/nginx/nginx.conf
sudo setsebool -P httpd_can_network_connect 1





























