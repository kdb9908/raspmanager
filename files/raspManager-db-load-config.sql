

-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for osx10.11 (x86_64)
--
-- Host: localhost    Database: prevoty
-- ------------------------------------------------------
-- Server version	10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `api_keys`
--

DROP TABLE IF EXISTS `api_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_keys` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) unsigned NOT NULL,
  `access_key` varchar(40) NOT NULL DEFAULT '',
  `secret_key` varchar(40) NOT NULL DEFAULT '',
  `last_used` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_key` (`access_key`),
  KEY `organization_id` (`organization_id`),
  CONSTRAINT `api_keys_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configurations`
--

DROP TABLE IF EXISTS `configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configurations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `product_type` int(1) unsigned NOT NULL,
  `configuration_key` char(36) NOT NULL DEFAULT '',
  `configuration_type` int(1) DEFAULT NULL,
  `description` longtext,
  `data` longtext NOT NULL,
  `last_updated` datetime NOT NULL,
  `last_updated_user_id` int(11) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `configuration_key` (`configuration_key`),
  KEY `organization_id` (`organization_id`),
  KEY `organization_id_2` (`organization_id`,`product_type`),
  KEY `last_updated_user_id` (`last_updated_user_id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `configurations_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
  CONSTRAINT `configurations_ibfk_2` FOREIGN KEY (`last_updated_user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `configurations_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `api_key` varchar(50) NOT NULL DEFAULT '',
  `api_key_rotated` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `content` tinyint(1) NOT NULL DEFAULT '0',
  `query` tinyint(1) NOT NULL DEFAULT '0',
  `token` tinyint(1) NOT NULL DEFAULT '0',
  `crypto` tinyint(1) NOT NULL DEFAULT '0',
  `custom_pattern` tinyint(1) NOT NULL DEFAULT '0',
  `content_monitoring` tinyint(1) NOT NULL DEFAULT '0',
  `query_monitoring` tinyint(1) NOT NULL DEFAULT '0',
  `native_pattern` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key` (`api_key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_info`
--

DROP TABLE IF EXISTS `schema_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `version` char(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `servers`
--

DROP TABLE IF EXISTS `servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server_type` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `port` int(11) NOT NULL,
  `is_healthy` tinyint(1) NOT NULL,
  `last_healthy_time` datetime DEFAULT NULL,
  `last_health_check` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `address` (`address`,`port`,`server_type`),
  KEY `server_type` (`server_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sid` char(36) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL,
  `oid` int(11) NOT NULL,
  `mfa` tinyint(1) NOT NULL,
  `mfa_secret` varchar(16) NOT NULL DEFAULT '',
  `logged_in` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sid` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_organizations`
--

DROP TABLE IF EXISTS `user_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_organizations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `organization_id` int(11) unsigned NOT NULL,
  `role` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_idx` (`organization_id`,`user_id`),
  KEY `uid_idx` (`user_id`),
  CONSTRAINT `user_organizations_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
  CONSTRAINT `user_organizations_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) NOT NULL DEFAULT '',
  `last_name` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `verified` int(1) NOT NULL,
  `verification_code` varchar(50) NOT NULL DEFAULT '',
  `password_reset_code` varchar(50) NOT NULL DEFAULT '',
  `mfa` tinyint(1) NOT NULL,
  `mfa_secret` varchar(16) NOT NULL DEFAULT '',
  `superadmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `verification_code` (`verification_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-17  8:59:20

SET FOREIGN_KEY_CHECKS=0;

--
-- Dumping data for table `user_organizations`
--

INSERT INTO `user_organizations` VALUES (1,1,1,1);

--
-- Dumping data for table `users`
--

INSERT INTO `users` VALUES (1,'Imperva','Admin','admin@imperva.com','$2a$10$2QCiQnc6uAMy6AbmE3bsT.dHqZZQCtUnPd1GI2KSzxi/MuEyT8MDa','2016-07-07 14:48:10','2019-07-18 20:36:22',1,'','',0,'',1);

--
-- Dumping data for table `api_keys`
--

INSERT INTO `api_keys` VALUES (1,1,'b64bba57-3c0d-4692-a993-f93696a0c81e','DwB1dQkSnD6+o0ULFLCkCw==','2016-08-17 00:04:53','2016-07-07 14:48:10');

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` VALUES (1,1,'Default',1,'ef99c002-3fe5-4814-aa34-01fb493c06fc',1,'[]','{\"BlacklistedPhrases\":[],\"FlaggedPhrases\":[],\"UniversalAttributes\":[],\"UniversalProtocols\":[],\"Tags\":{},\"RichMedia\":{},\"WhitelistedJavaScriptUrls\":[],\"SkipTagBalancing\":false,\"RunPrevotySpamDetection\":false,\"RunPrevotyProfanityDetection\":false,\"RunPrevotyLinkAnalysis\":false,\"PlaintextMode\":false}','2019-01-26 01:58:48',1,'2019-01-26 01:58:48',1,0);
INSERT INTO `configurations` VALUES (2,1,'Acme',2,'c6299afb-06df-44c0-823d-c3f00ff63158',2,'','{\"trustedquery_engine\":\"2.0\",\"vendor\":\"mysql\",\"vendor_version\":\"5.4\",\"databases\":{\"acme\":{\"tables\":{\"stocks\":{\"columns\":{\"id\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"name\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":4,\"update\":1,\"delete\":1,\"admin\":1,\"join\":1,\"union\":1,\"subquery\":1,\"functions\":[]}},\"users\":{\"columns\":{\"id\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"name\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"password\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"ssn\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":1,\"update\":1,\"delete\":1,\"admin\":1,\"join\":1,\"union\":1,\"subquery\":1,\"functions\":[]}},\"secrettable\":{\"columns\":{\"id\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"name\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":1,\"update\":1,\"delete\":1,\"admin\":1,\"join\":1,\"union\":1,\"subquery\":1,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":2,\"update\":1,\"delete\":1,\"admin\":1,\"join\":1,\"union\":1,\"subquery\":1,\"functions\":[]}}}}','2016-07-07 14:48:10',1,'2016-07-07 14:48:10',1,0);
INSERT INTO `configurations` VALUES (4,1,'Superveda',2,'a84682f9-1c5c-4764-8a68-7ffecd79d15a',NULL,NULL,'{\"trustedquery_engine\":\"2.0\",\"vendor\":\"mysql\",\"vendor_version\":\"\",\"policy_key\":\"\",\"databases\":{\"superveda_db\":{\"tables\":{\"adminusers\":{\"columns\":{\"apassword\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"auserid\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"ausername\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":1,\"update\":1,\"delete\":1,\"admin\":1,\"join\":1,\"union\":1,\"subquery\":1,\"functions\":[]}},\"categories\":{\"columns\":{\"catdesc\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"catid\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"catimage\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"catname\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":4,\"update\":1,\"delete\":1,\"admin\":1,\"join\":4,\"union\":1,\"subquery\":1,\"functions\":[]}},\"countries\":{\"columns\":{\"countryid\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"countryname\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"hasstates\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":4,\"update\":1,\"delete\":1,\"admin\":1,\"join\":4,\"union\":1,\"subquery\":1,\"functions\":[]}},\"messages\":{\"columns\":{\"msgid\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"msgsubj\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"msgtext\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"username\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":1,\"update\":1,\"delete\":1,\"admin\":1,\"join\":1,\"union\":1,\"subquery\":1,\"functions\":[]}},\"orders\":{\"columns\":{\"address\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"ccdate\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"ccnumber\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"country\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"firstname\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"lastname\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"orderdate\":{\"permissions\":{\"insert\":0,\"read\":4,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"orderid\":{\"permissions\":{\"insert\":0,\"read\":4,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"shippingdate\":{\"permissions\":{\"insert\":0,\"read\":4,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"state\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"status\":{\"permissions\":{\"insert\":0,\"read\":4,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"totalsum\":{\"permissions\":{\"insert\":0,\"read\":4,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"userid\":{\"permissions\":{\"insert\":0,\"read\":4,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":2,\"update\":1,\"delete\":1,\"admin\":1,\"join\":1,\"union\":1,\"subquery\":1,\"functions\":[]}},\"prodsinorder\":{\"columns\":{\"itemid\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"orderid\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"prodid\":{\"permissions\":{\"insert\":0,\"read\":4,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"quantity\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":2,\"update\":1,\"delete\":1,\"admin\":1,\"join\":4,\"union\":1,\"subquery\":1,\"functions\":[]}},\"products\":{\"columns\":{\"catid\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"proddesc\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"prodid\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"prodimage\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"prodname\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"prodprice\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"prodquant\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":4,\"update\":1,\"delete\":1,\"admin\":1,\"join\":4,\"union\":1,\"subquery\":1,\"functions\":[]}},\"states\":{\"columns\":{\"countryid\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"stateid\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}},\"statename\":{\"permissions\":{\"insert\":0,\"read\":1,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":1,\"update\":1,\"delete\":1,\"admin\":1,\"join\":1,\"union\":1,\"subquery\":1,\"functions\":[]}},\"usercomments\":{\"columns\":{\"comid\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"commsubj\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"commtext\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"prodid\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"prodrating\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"userid\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":4,\"update\":1,\"delete\":1,\"admin\":1,\"join\":4,\"union\":1,\"subquery\":1,\"functions\":[]}},\"users\":{\"columns\":{\"address\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"ccdate\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"ccnumber\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"country\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"email\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"firstname\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"generic\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"lastname\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"password\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"phonenum\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"state\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"userid\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}},\"username\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":3,\"union\":0,\"subquery\":0,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":4,\"update\":1,\"delete\":1,\"admin\":1,\"join\":4,\"union\":1,\"subquery\":1,\"functions\":[]}},\"voucher\":{\"columns\":{\"public\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":3,\"functions\":[]}},\"usesleft\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":3,\"functions\":[]}},\"value\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":3,\"functions\":[]}},\"vkey\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":3,\"functions\":[]}},\"voucherid\":{\"permissions\":{\"insert\":0,\"read\":3,\"update\":1,\"delete\":0,\"admin\":1,\"join\":0,\"union\":0,\"subquery\":3,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":4,\"update\":1,\"delete\":1,\"admin\":1,\"join\":1,\"union\":1,\"subquery\":4,\"functions\":[]}}},\"permissions\":{\"insert\":1,\"read\":2,\"update\":1,\"delete\":1,\"admin\":1,\"join\":2,\"union\":0,\"subquery\":2,\"functions\":[]}}}}','2019-07-12 00:07:15',1,'2019-07-11 21:23:45',1,0);
INSERT INTO `configurations` VALUES (5,1,'Acme - disabled',14,'605747b6-abc9-47a2-9c8d-2fea832bf849',NULL,'','{\"application\":{\"dependencyTrackingInitialMinutes\":15,\"dependencyTrackingIntervalMinutes\":60,\"filterPosition\":\"0\",\"javaClassesToIgnore\":[],\"name\":\"Acme - disabled\",\"trackDependencies\":true,\"whitelistedIPAddresses\":[],\"whitelistedUserAgents\":[]},\"commandInjection\":{\"httpOnly\":true,\"mode\":\"disabled\",\"whitelist\":[\"/usr/bin/tr\"]},\"content\":{\"allowedContentTypes\":[\"application/x-www-form-urlencoded\",\"application/json\",\"application/soap+xml\",\"application/xhtml+xml\",\"application/xml\",\"text/html\",\"text/plain\",\"text/xml\"],\"allowMissingContentType\":false,\"base64FormEncodedVars\":{},\"blockedHttpMethods\":{},\"canonicalization\":{\"html\":true,\"url\":true},\"canonicalizerIterations\":10,\"contentTypeProtectionMode\":\"dropRequest\",\"defaultCharacterEncoding\":\"UTF-8\",\"ignoredHttpMethods\":{},\"ignoredHttpVariables\":{},\"ignoredJSONKeys\":{},\"ignoredXMLTags\":{},\"javascriptVarsToEscape\":{},\"jsonFormEncodedVars\":[],\"maximumJavaScriptDomEventHandlerLoops\":5,\"mode\":\"disabled\",\"policy\":null,\"policyKey\":\"\",\"preventJavaScriptDomEventHandlers\":true,\"preventOGNL\":true,\"preventXXE\":true,\"protectAction\":\"sanitize\",\"protectHeaders\":[],\"whitelist\":[],\"xmlFormEncodedVars\":[]},\"csrf\":{\"encryptionKey\":\"SdJN/5EY8N8QeGj8bUQAIQ==\",\"enforce\":true,\"invalidationMode\":\"continue\",\"javascriptWhitelist\":[],\"mode\":\"disabled\",\"permitCrossOrigin\":false,\"permittedOrigins\":[],\"protectSafe\":false,\"safeExclusions\":[],\"secure\":false,\"whitelist\":[]},\"http\":{\"basicAuthURL\":\"\",\"cacheControlHeader\":\"\",\"cspHeader\":\"\",\"enforceCacheControlHeader\":false,\"enforceCSPHeader\":false,\"enforceHTTPS\":false,\"enforceXFrameOptionsHeader\":false,\"enforceXSSAuditorHeader\":false,\"forwardedForHeaders\":[\"X-Forwarded-For\",\"HTTP_X_FORWARDED_FOR\",\"Forwarded\",\"Client-IP\",\"Proxy-Client-IP\",\"true-client-ip\"],\"preventBasicAuth\":false,\"preventResponseSplitting\":false,\"preventUnvalidatedRedirects\":false,\"uncaughtExceptionsMode\":\"disabled\",\"uncaughtExceptionsContent\":\"\",\"uncaughtExceptionsContentType\":\"application/json\",\"uncaughtExceptionsStatusCode\":500,\"uncaughtExceptionsURL\":\"\",\"unvalidatedRedirectURL\":\"\",\"validRedirectHosts\":[],\"xFrameOptionsHeader\":\"deny\",\"xFrameOptionsOrigin\":\"\",\"xssAuditorAction\":\"block\",\"limitRequestSize\":false,\"maxRequestSize\":8192,\"enableStrictResponseBodyBuffering\":false},\"httpCookie\":{\"mode\":\"disabled\",\"defaultHttpOnly\":true,\"defaultSecure\":false,\"defaultDomain\":\"\",\"defaultPath\":\"\",\"defaultMaxAge\":3600,\"defaultExpires\":\"\",\"customCookies\":{}},\"logging\":{\"customDataMasks\":{},\"loggingType\":{\"json\":true,\"leef\":false,\"splunk\":false},\"logSeverity\":\"medium\",\"logVerbosity\":\"incident\",\"maskedJSONKeys\":{},\"maskedXMLTags\":{},\"piiMaskingFlags\":{\"creditCard\":true,\"email\":true},\"stackTraceDepth\":-1,\"statisticsIntervalMinutes\":1,\"logCookies\":true,\"logSessionAttributes\":true,\"logEnvironmentVariables\":true},\"prevotyMetadata\":null,\"networkActivity\":{\"mode\":\"disabled\",\"httpOnly\":true,\"tcpHostsAndPortsToIgnore\":[{\"port\":\"12\",\"host\":\"acme-daytime\"}]},\"pathTraversal\":{\"autoWhitelistClasspathsFrameworkPaths\":true,\"httpOnly\":true,\"mode\":\"disabled\",\"sharedTemporaryDirectories\":[\"/tmp\",\"/var/tmp\",\"c:\\\\temp\"],\"whitelist\":[\"/usr/local/tomcat/webapps/acme-webapp-java\",\"/usr/local/tomcat/work/Catalina/localhost/acme-webapp-java\",\"C:\\\\temp\\\\shared.txt\",\"/tmp/safe-to-read.txt\"]},\"query\":{\"blockOnComment\":true,\"blockOnContradiction\":true,\"blockOnMultipleStatements\":true,\"blockOnStaticComparison\":true,\"blockOnTautology\":true,\"blockOnViolation\":true,\"db2\":null,\"dbVendor\":{\"db2\":false,\"mssql\":false,\"mysql\":true,\"oracle\":false,\"postgresql\":false},\"failureMode\":\"continue\",\"httpOnly\":true,\"mode\":\"disabled\",\"mssql\":null,\"mysql\":null,\"oracle\":null,\"postgresql\":null,\"policyKeys\":{\"mysql\":\"c6299afb-06df-44c0-823d-c3f00ff63158\"}},\"weakCryptography\":{\"hashingAlgorithms\":{\"md2\":\"disabled\",\"md4\":\"disabled\",\"md5\":\"disabled\",\"sha-0\":\"disabled\",\"sha-1\":\"disabled\"},\"symmetricKeyCiphers\":{\"aes\":\"disabled\",\"blowfish\":\"disabled\",\"des\":\"disabled\",\"rc2\":\"disabled\",\"rc4\":\"disabled\",\"tripleDes\":\"disabled\"},\"classesToIgnore\":[]}}','2019-07-11 18:43:26',1,'2019-07-11 18:36:31',1,1);
INSERT INTO `configurations` VALUES (6,1,'Acme - monitor',14,'55fdd70c-492f-40b1-96ea-d4b4f11a6086',NULL,'','{\"application\":{\"dependencyTrackingInitialMinutes\":15,\"dependencyTrackingIntervalMinutes\":60,\"filterPosition\":\"0\",\"javaClassesToIgnore\":[],\"name\":\"Acme - monitor\",\"trackDependencies\":true,\"whitelistedIPAddresses\":[],\"whitelistedUserAgents\":[]},\"commandInjection\":{\"httpOnly\":true,\"mode\":\"monitor\",\"whitelist\":[\"/usr/bin/tr\"]},\"content\":{\"allowedContentTypes\":[\"application/x-www-form-urlencoded\",\"application/json\",\"application/soap+xml\",\"application/xhtml+xml\",\"application/xml\",\"text/html\",\"text/plain\",\"text/xml\"],\"allowMissingContentType\":false,\"base64FormEncodedVars\":{},\"blockedHttpMethods\":{},\"canonicalization\":{\"html\":true,\"url\":true},\"canonicalizerIterations\":10,\"contentTypeProtectionMode\":\"dropRequest\",\"defaultCharacterEncoding\":\"UTF-8\",\"ignoredHttpMethods\":{},\"ignoredHttpVariables\":{},\"ignoredJSONKeys\":{},\"ignoredXMLTags\":{},\"javascriptVarsToEscape\":{},\"jsonFormEncodedVars\":[],\"maximumJavaScriptDomEventHandlerLoops\":5,\"mode\":\"monitor\",\"policy\":null,\"policyKey\":\"ef99c002-3fe5-4814-aa34-01fb493c06fc\",\"preventJavaScriptDomEventHandlers\":true,\"preventOGNL\":true,\"preventXXE\":true,\"protectAction\":\"sanitize\",\"protectHeaders\":[],\"whitelist\":[],\"xmlFormEncodedVars\":[]},\"csrf\":{\"encryptionKey\":\"SdJN/5EY8N8QeGj8bUQAIQ==\",\"enforce\":false,\"invalidationMode\":\"continue\",\"javascriptWhitelist\":[],\"mode\":\"monitor\",\"permitCrossOrigin\":false,\"permittedOrigins\":[],\"protectSafe\":false,\"safeExclusions\":[],\"secure\":false,\"whitelist\":[]},\"http\":{\"basicAuthURL\":\"\",\"cacheControlHeader\":\"\",\"cspHeader\":\"\",\"enforceCacheControlHeader\":false,\"enforceCSPHeader\":false,\"enforceHTTPS\":false,\"enforceXFrameOptionsHeader\":false,\"enforceXSSAuditorHeader\":false,\"forwardedForHeaders\":[\"X-Forwarded-For\",\"HTTP_X_FORWARDED_FOR\",\"Forwarded\",\"Client-IP\",\"Proxy-Client-IP\",\"true-client-ip\"],\"preventBasicAuth\":false,\"preventResponseSplitting\":false,\"preventUnvalidatedRedirects\":false,\"uncaughtExceptionsMode\":\"disabled\",\"uncaughtExceptionsContent\":\"\",\"uncaughtExceptionsContentType\":\"application/json\",\"uncaughtExceptionsStatusCode\":500,\"uncaughtExceptionsURL\":\"\",\"unvalidatedRedirectURL\":\"\",\"validRedirectHosts\":[],\"xFrameOptionsHeader\":\"deny\",\"xFrameOptionsOrigin\":\"\",\"xssAuditorAction\":\"block\",\"limitRequestSize\":false,\"maxRequestSize\":8192,\"enableStrictResponseBodyBuffering\":false},\"httpCookie\":{\"mode\":\"monitor\",\"defaultHttpOnly\":true,\"defaultSecure\":false,\"defaultDomain\":\"\",\"defaultPath\":\"\",\"defaultMaxAge\":3600,\"defaultExpires\":\"\",\"customCookies\":{}},\"logging\":{\"customDataMasks\":{},\"loggingType\":{\"json\":true,\"leef\":false,\"splunk\":false},\"logSeverity\":\"medium\",\"logVerbosity\":\"incident\",\"maskedJSONKeys\":{},\"maskedXMLTags\":{},\"piiMaskingFlags\":{\"creditCard\":true,\"email\":true},\"stackTraceDepth\":-1,\"statisticsIntervalMinutes\":1,\"logCookies\":true,\"logSessionAttributes\":true,\"logEnvironmentVariables\":true},\"prevotyMetadata\":null,\"networkActivity\":{\"mode\":\"monitor\",\"httpOnly\":true,\"tcpHostsAndPortsToIgnore\":[{\"port\":\"12\",\"host\":\"acme-daytime\"}]},\"pathTraversal\":{\"autoWhitelistClasspathsFrameworkPaths\":true,\"httpOnly\":true,\"mode\":\"monitor\",\"sharedTemporaryDirectories\":[\"/tmp\",\"/var/tmp\",\"c:\\\\temp\"],\"whitelist\":[\"/usr/local/tomcat/webapps/acme-webapp-java\",\"/usr/local/tomcat/work/Catalina/localhost/acme-webapp-java\",\"C:\\\\temp\\\\shared.txt\",\"/tmp/safe-to-read.txt\"]},\"query\":{\"blockOnComment\":true,\"blockOnContradiction\":true,\"blockOnMultipleStatements\":true,\"blockOnStaticComparison\":true,\"blockOnTautology\":true,\"blockOnViolation\":true,\"db2\":null,\"dbVendor\":{\"db2\":false,\"mssql\":false,\"mysql\":true,\"oracle\":false,\"postgresql\":false},\"failureMode\":\"continue\",\"httpOnly\":true,\"mode\":\"monitor\",\"mssql\":null,\"mysql\":null,\"oracle\":null,\"postgresql\":null,\"policyKeys\":{\"mysql\":\"c6299afb-06df-44c0-823d-c3f00ff63158\"}},\"weakCryptography\":{\"hashingAlgorithms\":{\"md2\":\"monitor\",\"md4\":\"monitor\",\"md5\":\"monitor\",\"sha-0\":\"monitor\",\"sha-1\":\"monitor\"},\"symmetricKeyCiphers\":{\"aes\":\"monitor\",\"blowfish\":\"monitor\",\"des\":\"monitor\",\"rc2\":\"monitor\",\"rc4\":\"monitor\",\"tripleDes\":\"monitor\"},\"classesToIgnore\":[]}}','2019-07-11 20:58:01',1,'2019-07-11 17:48:07',1,1);
INSERT INTO `configurations` VALUES (7,1,'Acme - protect',14,'93cbbd9b-d844-4b83-850a-e52f0df1badd',NULL,'','{\"application\":{\"dependencyTrackingInitialMinutes\":15,\"dependencyTrackingIntervalMinutes\":60,\"filterPosition\":\"0\",\"javaClassesToIgnore\":[],\"name\":\"Acme - protect\",\"trackDependencies\":true,\"whitelistedIPAddresses\":[],\"whitelistedUserAgents\":[]},\"commandInjection\":{\"httpOnly\":true,\"mode\":\"protect\",\"whitelist\":[\"/usr/bin/tr\"]},\"content\":{\"allowedContentTypes\":[\"application/x-www-form-urlencoded\",\"application/json\",\"application/soap+xml\",\"application/xhtml+xml\",\"application/xml\",\"text/html\",\"text/plain\",\"text/xml\"],\"allowMissingContentType\":false,\"base64FormEncodedVars\":{},\"blockedHttpMethods\":{},\"canonicalization\":{\"html\":true,\"url\":true},\"canonicalizerIterations\":10,\"contentTypeProtectionMode\":\"dropRequest\",\"defaultCharacterEncoding\":\"UTF-8\",\"ignoredHttpMethods\":{\"/acme-webapp-java/ignore.jsp\":[\"GET\"],\"/acme-webapp-java/page.jsp\":[\"GET\"]},\"ignoredHttpVariables\":{\"/acme-webapp-java/ignore_vars.jsp\":[\"foo\"]},\"ignoredJSONKeys\":{},\"ignoredXMLTags\":{},\"javascriptVarsToEscape\":{},\"jsonFormEncodedVars\":[\"_JSON_\"],\"maximumJavaScriptDomEventHandlerLoops\":5,\"mode\":\"protect\",\"policy\":null,\"policyKey\":\"ef99c002-3fe5-4814-aa34-01fb493c06fc\",\"preventJavaScriptDomEventHandlers\":true,\"preventOGNL\":true,\"preventXXE\":true,\"protectAction\":\"sanitize\",\"protectHeaders\":[],\"whitelist\":[],\"xmlFormEncodedVars\":[]},\"csrf\":{\"encryptionKey\":\"SdJN/5EY8N8QeGj8bUQAIQ==\",\"enforce\":true,\"invalidationMode\":\"continue\",\"javascriptWhitelist\":[],\"mode\":\"protect\",\"permitCrossOrigin\":false,\"permittedOrigins\":[],\"protectSafe\":false,\"safeExclusions\":[],\"secure\":false,\"whitelist\":[]},\"http\":{\"basicAuthURL\":\"\",\"cacheControlHeader\":\"\",\"cspHeader\":\"\",\"enforceCacheControlHeader\":false,\"enforceCSPHeader\":false,\"enforceHTTPS\":false,\"enforceXFrameOptionsHeader\":false,\"enforceXSSAuditorHeader\":false,\"forwardedForHeaders\":[\"X-Forwarded-For\",\"HTTP_X_FORWARDED_FOR\",\"Forwarded\",\"Client-IP\",\"Proxy-Client-IP\",\"true-client-ip\"],\"preventBasicAuth\":false,\"preventResponseSplitting\":false,\"preventUnvalidatedRedirects\":false,\"uncaughtExceptionsMode\":\"disabled\",\"uncaughtExceptionsContent\":\"\",\"uncaughtExceptionsContentType\":\"application/json\",\"uncaughtExceptionsStatusCode\":500,\"uncaughtExceptionsURL\":\"\",\"unvalidatedRedirectURL\":\"\",\"validRedirectHosts\":[],\"xFrameOptionsHeader\":\"deny\",\"xFrameOptionsOrigin\":\"\",\"xssAuditorAction\":\"block\",\"limitRequestSize\":false,\"maxRequestSize\":8192,\"enableStrictResponseBodyBuffering\":false},\"httpCookie\":{\"mode\":\"monitor\",\"defaultHttpOnly\":true,\"defaultSecure\":false,\"defaultDomain\":\"\",\"defaultPath\":\"\",\"defaultMaxAge\":3600,\"defaultExpires\":\"\",\"customCookies\":{}},\"logging\":{\"customDataMasks\":{},\"loggingType\":{\"json\":true,\"leef\":false,\"splunk\":false},\"logSeverity\":\"medium\",\"logVerbosity\":\"incident\",\"maskedJSONKeys\":{},\"maskedXMLTags\":{},\"piiMaskingFlags\":{\"creditCard\":true,\"email\":true},\"stackTraceDepth\":-1,\"statisticsIntervalMinutes\":1,\"logCookies\":true,\"logSessionAttributes\":true,\"logEnvironmentVariables\":true},\"prevotyMetadata\":null,\"networkActivity\":{\"mode\":\"protect\",\"httpOnly\":true,\"tcpHostsAndPortsToIgnore\":[{\"port\":\"12\",\"host\":\"acme-daytime\"}]},\"pathTraversal\":{\"autoWhitelistClasspathsFrameworkPaths\":true,\"httpOnly\":true,\"mode\":\"protect\",\"sharedTemporaryDirectories\":[\"/tmp\",\"/var/tmp\",\"c:\\\\temp\"],\"whitelist\":[\"/usr/local/tomcat/webapps/acme-webapp-java\",\"/usr/local/tomcat/work/Catalina/localhost/acme-webapp-java\",\"C:\\\\temp\\\\shared.txt\",\"/tmp/safe-to-read.txt\"]},\"query\":{\"blockOnComment\":true,\"blockOnContradiction\":true,\"blockOnMultipleStatements\":true,\"blockOnStaticComparison\":true,\"blockOnTautology\":true,\"blockOnViolation\":true,\"db2\":null,\"dbVendor\":{\"db2\":false,\"mssql\":false,\"mysql\":true,\"oracle\":false,\"postgresql\":false},\"failureMode\":\"continue\",\"httpOnly\":true,\"mode\":\"protect\",\"mssql\":null,\"mysql\":null,\"oracle\":null,\"postgresql\":null,\"policyKeys\":{\"mysql\":\"c6299afb-06df-44c0-823d-c3f00ff63158\"}},\"weakCryptography\":{\"hashingAlgorithms\":{\"md2\":\"monitor\",\"md4\":\"monitor\",\"md5\":\"monitor\",\"sha-0\":\"monitor\",\"sha-1\":\"monitor\"},\"symmetricKeyCiphers\":{\"aes\":\"monitor\",\"blowfish\":\"monitor\",\"des\":\"monitor\",\"rc2\":\"monitor\",\"rc4\":\"monitor\",\"tripleDes\":\"monitor\"},\"classesToIgnore\":[]}}','2019-07-17 21:50:04',1,'2019-07-11 17:42:39',1,1);
INSERT INTO `configurations` VALUES (11,1,'Superveda - disabled',14,'8772230d-8ba8-4f46-b172-0ca69d06a8d1',NULL,'','{\"application\":{\"dependencyTrackingInitialMinutes\":15,\"dependencyTrackingIntervalMinutes\":60,\"filterPosition\":\"0\",\"javaClassesToIgnore\":[],\"name\":\"Superveda - disabled\",\"trackDependencies\":false,\"whitelistedIPAddresses\":[],\"whitelistedUserAgents\":[]},\"commandInjection\":{\"httpOnly\":true,\"mode\":\"disabled\",\"whitelist\":[]},\"content\":{\"allowedContentTypes\":[\"application/x-www-form-urlencoded\",\"application/json\",\"application/soap+xml\",\"application/xhtml+xml\",\"application/xml\",\"text/html\",\"text/plain\",\"text/xml\"],\"allowMissingContentType\":false,\"base64FormEncodedVars\":{},\"blockedHttpMethods\":{},\"canonicalization\":{\"html\":true,\"url\":true},\"canonicalizerIterations\":10,\"contentTypeProtectionMode\":\"disabled\",\"defaultCharacterEncoding\":\"UTF-8\",\"ignoredHttpMethods\":{},\"ignoredHttpVariables\":{},\"ignoredJSONKeys\":{},\"ignoredXMLTags\":{},\"javascriptVarsToEscape\":{},\"jsonFormEncodedVars\":[],\"maximumJavaScriptDomEventHandlerLoops\":5,\"mode\":\"disabled\",\"policy\":null,\"policyKey\":\"\",\"preventJavaScriptDomEventHandlers\":false,\"preventOGNL\":false,\"preventXXE\":false,\"protectAction\":\"sanitize\",\"protectHeaders\":[],\"whitelist\":[],\"xmlFormEncodedVars\":[]},\"csrf\":{\"encryptionKey\":\"KhqQF48IfGnyRCcewSEEzA==\",\"enforce\":false,\"invalidationMode\":\"continue\",\"javascriptWhitelist\":[],\"mode\":\"disabled\",\"permitCrossOrigin\":false,\"permittedOrigins\":[],\"protectSafe\":false,\"safeExclusions\":[],\"secure\":false,\"whitelist\":[]},\"http\":{\"basicAuthURL\":\"\",\"cacheControlHeader\":\"\",\"cspHeader\":\"\",\"enforceCacheControlHeader\":false,\"enforceCSPHeader\":false,\"enforceHTTPS\":false,\"enforceXFrameOptionsHeader\":false,\"enforceXSSAuditorHeader\":false,\"forwardedForHeaders\":[\"X-Forwarded-For\",\"HTTP_X_FORWARDED_FOR\",\"Forwarded\",\"Client-IP\",\"Proxy-Client-IP\",\"true-client-ip\"],\"preventBasicAuth\":false,\"preventResponseSplitting\":false,\"preventUnvalidatedRedirects\":false,\"uncaughtExceptionsMode\":\"disabled\",\"uncaughtExceptionsContent\":\"\",\"uncaughtExceptionsContentType\":\"application/json\",\"uncaughtExceptionsStatusCode\":500,\"uncaughtExceptionsURL\":\"\",\"unvalidatedRedirectURL\":\"\",\"validRedirectHosts\":[],\"xFrameOptionsHeader\":\"deny\",\"xFrameOptionsOrigin\":\"\",\"xssAuditorAction\":\"block\",\"limitRequestSize\":false,\"maxRequestSize\":8192,\"enableStrictResponseBodyBuffering\":false},\"httpCookie\":{\"mode\":\"disabled\",\"defaultHttpOnly\":false,\"defaultSecure\":false,\"defaultDomain\":\"\",\"defaultPath\":\"\",\"defaultMaxAge\":null,\"defaultExpires\":\"\",\"customCookies\":{}},\"logging\":{\"customDataMasks\":{},\"loggingType\":{\"json\":true,\"leef\":false,\"splunk\":false},\"logSeverity\":\"medium\",\"logVerbosity\":\"incident\",\"maskedJSONKeys\":{},\"maskedXMLTags\":{},\"piiMaskingFlags\":{\"creditCard\":false,\"email\":false},\"stackTraceDepth\":10,\"statisticsIntervalMinutes\":1,\"logCookies\":true,\"logSessionAttributes\":true,\"logEnvironmentVariables\":true},\"prevotyMetadata\":null,\"networkActivity\":{\"mode\":\"disabled\",\"httpOnly\":false,\"tcpHostsAndPortsToIgnore\":[]},\"pathTraversal\":{\"autoWhitelistClasspathsFrameworkPaths\":false,\"httpOnly\":true,\"mode\":\"disabled\",\"sharedTemporaryDirectories\":[\"/tmp\",\"/var/tmp\",\"c:\\\\temp\"],\"whitelist\":[]},\"query\":{\"blockOnComment\":false,\"blockOnContradiction\":false,\"blockOnMultipleStatements\":false,\"blockOnStaticComparison\":false,\"blockOnTautology\":false,\"blockOnViolation\":false,\"db2\":null,\"dbVendor\":{\"db2\":false,\"mssql\":false,\"mysql\":false,\"oracle\":false,\"postgresql\":false},\"failureMode\":\"continue\",\"httpOnly\":true,\"mode\":\"disabled\",\"mssql\":null,\"mysql\":null,\"oracle\":null,\"postgresql\":null,\"policyKeys\":{}},\"weakCryptography\":{\"hashingAlgorithms\":{\"md2\":\"disabled\",\"md4\":\"disabled\",\"md5\":\"disabled\",\"sha-0\":\"disabled\",\"sha-1\":\"disabled\"},\"symmetricKeyCiphers\":{\"aes\":\"disabled\",\"blowfish\":\"disabled\",\"des\":\"disabled\",\"rc2\":\"disabled\",\"rc4\":\"disabled\",\"tripleDes\":\"disabled\"},\"classesToIgnore\":[]}}','2019-07-12 00:07:25',1,'2019-07-11 21:24:19',1,1);
INSERT INTO `configurations` VALUES (12,1,'Superveda - monitor',14,'544c517d-0ce4-47e0-b593-348d7afa1060',NULL,'','{\"application\":{\"dependencyTrackingInitialMinutes\":15,\"dependencyTrackingIntervalMinutes\":60,\"filterPosition\":\"0\",\"javaClassesToIgnore\":[],\"name\":\"Superveda - monitor\",\"trackDependencies\":true,\"whitelistedIPAddresses\":[],\"whitelistedUserAgents\":[]},\"commandInjection\":{\"httpOnly\":true,\"mode\":\"monitor\",\"whitelist\":[]},\"content\":{\"allowedContentTypes\":[\"application/x-www-form-urlencoded\",\"application/json\",\"application/soap+xml\",\"application/xhtml+xml\",\"application/xml\",\"text/html\",\"text/plain\",\"text/xml\"],\"allowMissingContentType\":false,\"base64FormEncodedVars\":{},\"blockedHttpMethods\":{},\"canonicalization\":{\"html\":true,\"url\":true},\"canonicalizerIterations\":10,\"contentTypeProtectionMode\":\"dropRequest\",\"defaultCharacterEncoding\":\"UTF-8\",\"ignoredHttpMethods\":{},\"ignoredHttpVariables\":{},\"ignoredJSONKeys\":{},\"ignoredXMLTags\":{},\"javascriptVarsToEscape\":{},\"jsonFormEncodedVars\":[],\"maximumJavaScriptDomEventHandlerLoops\":5,\"mode\":\"monitor\",\"policy\":null,\"policyKey\":\"ef99c002-3fe5-4814-aa34-01fb493c06fc\",\"preventJavaScriptDomEventHandlers\":false,\"preventOGNL\":false,\"preventXXE\":true,\"protectAction\":\"sanitize\",\"protectHeaders\":[],\"whitelist\":[],\"xmlFormEncodedVars\":[]},\"csrf\":{\"encryptionKey\":\"KhqQF48IfGnyRCcewSEEzA==\",\"enforce\":false,\"invalidationMode\":\"continue\",\"javascriptWhitelist\":[],\"mode\":\"monitor\",\"permitCrossOrigin\":false,\"permittedOrigins\":[],\"protectSafe\":false,\"safeExclusions\":[],\"secure\":false,\"whitelist\":[]},\"http\":{\"basicAuthURL\":\"\",\"cacheControlHeader\":\"\",\"cspHeader\":\"\",\"enforceCacheControlHeader\":false,\"enforceCSPHeader\":false,\"enforceHTTPS\":false,\"enforceXFrameOptionsHeader\":false,\"enforceXSSAuditorHeader\":false,\"forwardedForHeaders\":[\"X-Forwarded-For\",\"HTTP_X_FORWARDED_FOR\",\"Forwarded\",\"Client-IP\",\"Proxy-Client-IP\",\"true-client-ip\"],\"preventBasicAuth\":false,\"preventResponseSplitting\":false,\"preventUnvalidatedRedirects\":false,\"uncaughtExceptionsMode\":\"disabled\",\"uncaughtExceptionsContent\":\"\",\"uncaughtExceptionsContentType\":\"application/json\",\"uncaughtExceptionsStatusCode\":500,\"uncaughtExceptionsURL\":\"\",\"unvalidatedRedirectURL\":\"\",\"validRedirectHosts\":[],\"xFrameOptionsHeader\":\"deny\",\"xFrameOptionsOrigin\":\"\",\"xssAuditorAction\":\"block\",\"limitRequestSize\":false,\"maxRequestSize\":8192,\"enableStrictResponseBodyBuffering\":false},\"httpCookie\":{\"mode\":\"disabled\",\"defaultHttpOnly\":false,\"defaultSecure\":false,\"defaultDomain\":\"\",\"defaultPath\":\"\",\"defaultMaxAge\":null,\"defaultExpires\":\"\",\"customCookies\":{}},\"logging\":{\"customDataMasks\":{},\"loggingType\":{\"json\":true,\"leef\":false,\"splunk\":false},\"logSeverity\":\"medium\",\"logVerbosity\":\"incident\",\"maskedJSONKeys\":{},\"maskedXMLTags\":{},\"piiMaskingFlags\":{\"creditCard\":true,\"email\":true},\"stackTraceDepth\":-1,\"statisticsIntervalMinutes\":1,\"logCookies\":true,\"logSessionAttributes\":true,\"logEnvironmentVariables\":true},\"prevotyMetadata\":null,\"networkActivity\":{\"mode\":\"monitor\",\"httpOnly\":true,\"tcpHostsAndPortsToIgnore\":[{\"port\":\"3306\",\"host\":\"mysql\"}]},\"pathTraversal\":{\"autoWhitelistClasspathsFrameworkPaths\":false,\"httpOnly\":true,\"mode\":\"monitor\",\"sharedTemporaryDirectories\":[\"/tmp\",\"/var/tmp\",\"c:\\\\temp\"],\"whitelist\":[\"/etc/java-8-openjdk/net.properties\",\"/usr/local/tomcat/webapps/ROOT\",\"/usr/local/tomcat/work/Catalina/localhost/ROOT\"]},\"query\":{\"blockOnComment\":true,\"blockOnContradiction\":true,\"blockOnMultipleStatements\":true,\"blockOnStaticComparison\":true,\"blockOnTautology\":true,\"blockOnViolation\":false,\"db2\":null,\"dbVendor\":{\"db2\":false,\"mssql\":false,\"mysql\":true,\"oracle\":false,\"postgresql\":false},\"failureMode\":\"block\",\"httpOnly\":true,\"mode\":\"monitor\",\"mssql\":null,\"mysql\":null,\"oracle\":null,\"postgresql\":null,\"policyKeys\":{\"mysql\":\"a84682f9-1c5c-4764-8a68-7ffecd79d15a\"}},\"weakCryptography\":{\"hashingAlgorithms\":{\"md2\":\"disabled\",\"md4\":\"disabled\",\"md5\":\"disabled\",\"sha-0\":\"disabled\",\"sha-1\":\"disabled\"},\"symmetricKeyCiphers\":{\"aes\":\"disabled\",\"blowfish\":\"disabled\",\"des\":\"disabled\",\"rc2\":\"disabled\",\"rc4\":\"disabled\",\"tripleDes\":\"disabled\"},\"classesToIgnore\":[]}}','2019-07-18 20:34:34',1,'2019-07-11 21:24:21',1,1);
INSERT INTO `configurations` VALUES (13,1,'Superveda - protect',14,'e969cecb-c534-434e-a521-547ae5b118da',NULL,'','{\"application\":{\"dependencyTrackingInitialMinutes\":15,\"dependencyTrackingIntervalMinutes\":60,\"filterPosition\":\"0\",\"javaClassesToIgnore\":[],\"name\":\"Superveda - protect\",\"trackDependencies\":true,\"whitelistedIPAddresses\":[],\"whitelistedUserAgents\":[]},\"commandInjection\":{\"httpOnly\":true,\"mode\":\"protect\",\"whitelist\":[]},\"content\":{\"allowedContentTypes\":[\"application/x-www-form-urlencoded\",\"application/json\",\"application/soap+xml\",\"application/xhtml+xml\",\"application/xml\",\"text/html\",\"text/plain\",\"text/xml\"],\"allowMissingContentType\":false,\"base64FormEncodedVars\":{},\"blockedHttpMethods\":{},\"canonicalization\":{\"html\":true,\"url\":true},\"canonicalizerIterations\":10,\"contentTypeProtectionMode\":\"dropRequest\",\"defaultCharacterEncoding\":\"UTF-8\",\"ignoredHttpMethods\":{},\"ignoredHttpVariables\":{},\"ignoredJSONKeys\":{},\"ignoredXMLTags\":{},\"javascriptVarsToEscape\":{},\"jsonFormEncodedVars\":[],\"maximumJavaScriptDomEventHandlerLoops\":5,\"mode\":\"protect\",\"policy\":null,\"policyKey\":\"ef99c002-3fe5-4814-aa34-01fb493c06fc\",\"preventJavaScriptDomEventHandlers\":false,\"preventOGNL\":false,\"preventXXE\":true,\"protectAction\":\"sanitize\",\"protectHeaders\":[],\"whitelist\":[],\"xmlFormEncodedVars\":[]},\"csrf\":{\"encryptionKey\":\"KhqQF48IfGnyRCcewSEEzA==\",\"enforce\":false,\"invalidationMode\":\"continue\",\"javascriptWhitelist\":[],\"mode\":\"protect\",\"permitCrossOrigin\":false,\"permittedOrigins\":[],\"protectSafe\":false,\"safeExclusions\":[],\"secure\":false,\"whitelist\":[]},\"http\":{\"basicAuthURL\":\"\",\"cacheControlHeader\":\"\",\"cspHeader\":\"\",\"enforceCacheControlHeader\":false,\"enforceCSPHeader\":false,\"enforceHTTPS\":false,\"enforceXFrameOptionsHeader\":false,\"enforceXSSAuditorHeader\":false,\"forwardedForHeaders\":[\"X-Forwarded-For\",\"HTTP_X_FORWARDED_FOR\",\"Forwarded\",\"Client-IP\",\"Proxy-Client-IP\",\"true-client-ip\"],\"preventBasicAuth\":false,\"preventResponseSplitting\":false,\"preventUnvalidatedRedirects\":false,\"uncaughtExceptionsMode\":\"disabled\",\"uncaughtExceptionsContent\":\"\",\"uncaughtExceptionsContentType\":\"application/json\",\"uncaughtExceptionsStatusCode\":500,\"uncaughtExceptionsURL\":\"\",\"unvalidatedRedirectURL\":\"\",\"validRedirectHosts\":[],\"xFrameOptionsHeader\":\"deny\",\"xFrameOptionsOrigin\":\"\",\"xssAuditorAction\":\"block\",\"limitRequestSize\":false,\"maxRequestSize\":8192,\"enableStrictResponseBodyBuffering\":false},\"httpCookie\":{\"mode\":\"disabled\",\"defaultHttpOnly\":false,\"defaultSecure\":false,\"defaultDomain\":\"\",\"defaultPath\":\"\",\"defaultMaxAge\":null,\"defaultExpires\":\"\",\"customCookies\":{}},\"logging\":{\"customDataMasks\":{},\"loggingType\":{\"json\":true,\"leef\":false,\"splunk\":false},\"logSeverity\":\"medium\",\"logVerbosity\":\"incident\",\"maskedJSONKeys\":{},\"maskedXMLTags\":{},\"piiMaskingFlags\":{\"creditCard\":true,\"email\":true},\"stackTraceDepth\":-1,\"statisticsIntervalMinutes\":1,\"logCookies\":true,\"logSessionAttributes\":true,\"logEnvironmentVariables\":true},\"prevotyMetadata\":null,\"networkActivity\":{\"mode\":\"monitor\",\"httpOnly\":true,\"tcpHostsAndPortsToIgnore\":[{\"port\":\"3306\",\"host\":\"mysql\"}]},\"pathTraversal\":{\"autoWhitelistClasspathsFrameworkPaths\":true,\"httpOnly\":true,\"mode\":\"protect\",\"sharedTemporaryDirectories\":[\"/tmp\",\"/var/tmp\",\"c:\\\\temp\"],\"whitelist\":[\"/etc/java-8-openjdk/net.properties\",\"/usr/local/tomcat/webapps/ROOT\",\"/usr/local/tomcat/work/Catalina/localhost/ROOT\"]},\"query\":{\"blockOnComment\":true,\"blockOnContradiction\":true,\"blockOnMultipleStatements\":true,\"blockOnStaticComparison\":true,\"blockOnTautology\":true,\"blockOnViolation\":false,\"db2\":null,\"dbVendor\":{\"db2\":false,\"mssql\":false,\"mysql\":true,\"oracle\":false,\"postgresql\":false},\"failureMode\":\"block\",\"httpOnly\":true,\"mode\":\"protect\",\"mssql\":null,\"mysql\":null,\"oracle\":null,\"postgresql\":null,\"policyKeys\":{\"mysql\":\"a84682f9-1c5c-4764-8a68-7ffecd79d15a\"}},\"weakCryptography\":{\"hashingAlgorithms\":{\"md2\":\"disabled\",\"md4\":\"disabled\",\"md5\":\"disabled\",\"sha-0\":\"disabled\",\"sha-1\":\"disabled\"},\"symmetricKeyCiphers\":{\"aes\":\"disabled\",\"blowfish\":\"disabled\",\"des\":\"disabled\",\"rc2\":\"disabled\",\"rc4\":\"disabled\",\"tripleDes\":\"disabled\"},\"classesToIgnore\":[]}}','2019-07-18 20:34:40',1,'2019-07-11 21:24:21',1,1);

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` VALUES (1,'Org','66026528-ef02-4e29-8be1-81cffbffd881',NULL,'2016-07-07 14:48:10',1,1,1,1,1,1,1,1);

--
-- Dumping data for table `schema_info`
--

INSERT INTO `schema_info` VALUES (1,'20150819001420');
INSERT INTO `schema_info` VALUES (2,'20151027001420');

--
-- Dumping data for table `servers`
--


--
-- Dumping data for table `sessions`
--

SET FOREIGN_KEY_CHECKS=1;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-18 20:36:59


